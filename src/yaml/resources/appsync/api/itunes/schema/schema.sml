type Api {
	catalog: Catalog
}

type Catalog {
	podcasts(term: String, limit: Int = 100): [Entity]
}

type Entity {
	wrapperType: String
	kind: String
	artistId: ID
	collectionId: ID
	trackId: ID
	artistName: String
	collectionName: String
	trackName: String
	collectionCensoredName: String
	trackCensoredName: String
	artistViewUrl: String
	collectionViewUrl: String
	feedUrl: String
	trackViewUrl: String
	artworkUrl30: String
	artworkUrl60: String
	artworkUrl100: String
	artworkUrl600: String
	collectionPrice: Float
	trackPrice: Float
	trackRentalPrice: Float
	collectionHdPrice: Float
	trackHdPrice: Float
	trackHdRentalPrice: Float
	releaseDate: String
	collectionExplicitness: String
	trackExplicitness: String
	trackCount: Int
	country: String
	currency: String
	primaryGenreName: String
	genreIds: [ID]
	genres: [String]
}

type Query {
	itunes: Api
}

schema {
	query: Query
}
